import {reactive} from 'vue';

export function itemStore() {

    const getUrl = path => `http://localhost/admin/api${path}`;

    const state = reactive({
        items: [],
        links: [],
    });


    const getItems = async function(page) {
        try {
            const res = await fetch(getUrl(`/item/list?page=${page}`));
            if(!res.ok) {
                throw new Error('通信エラー');
            }
            const jsonData = await res.json();
            state.items = jsonData.data;
            state.links = jsonData.links;

        } catch (error) {
            alert("通信エラー\n再度ページを読み込み直してください。");
        }
    }

    const getItem = async function(itemId) {

        const itemData = {
            data: {},
            images: {},
        };
        try {
            const res = await fetch(getUrl(`/item/${itemId}`));
            if(!res.ok) {
                throw new Error('通信エラー');
            }
            const jsonData = await res.json();
            itemData.data = jsonData.data;
            itemData.images = jsonData.images;
        } catch (error) {
            alert("通信エラー\n再度ページを読み込み直してください。");
        }

        return itemData;
    }

    const updateItem = async function(item, page) {
        try {
            const res = await fetch(getUrl(`/item/${item.id}`), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            })
            if(!res.ok) {
                throw new Error('通信エラー');
            }
            getItems(page);
        } catch (error) {
            alert("通信エラー\n再度ページを読み込み直してください。");
        }
    }

    getItems(1);

    return {
        state,
        getItems,
        updateItem,
        getItem
    }
}