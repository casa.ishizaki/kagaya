<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Item;
use App\Models\ItemImage;

class AdminController extends Controller
{
    //
    function itemList(Request $request) {

        $page = $request->page - 1;
        $page_limit = 5;
        $page_offset = $page * $page_limit;


        $result_customers = Customer::select()->offset($page_offset)->limit($page_limit)->get();
        $customers = [];
        // $items = [];
        foreach($result_customers as $customer) {
            $customer->items;
            $customers[] = $customer;
        }

        $count = Customer::count();
        $page_max = ceil($count / $page_limit);

        $links = [];
        for($i=1; $i<=$page_max; $i++) {
            $is_active = false;
            if ($i == $page + 1) {
                $is_active = true;
            }
            $links[] = ['label'=> $i, 'active' => $is_active];
        }


        $return_array = ['data' => $customers, 'links' => $links];

        return $return_array;
    }

    function itemUpdate($item_id, Request $request) {
        $item = Item::find($item_id);
        $item->status = $request->status;
        $item->save();
        return $item;
    }

    function getItem($id) {
        $customer = Customer::find($id);
        $customer->items;
        $images = ItemImage::where('item_id', $customer->items[0]->id)->get();
        $resultArray = ['data' => $customer, 'images' => $images];

        return $resultArray;
    }

    function cutomerList() {
        return Customer::all();
    }
}
