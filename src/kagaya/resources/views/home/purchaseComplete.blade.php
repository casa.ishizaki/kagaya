@extends('layout.home')

@section('title', '古物商 加賀谷 店主紹介')
@section('description', '古物商 加賀谷 店主紹介')
@section('keyword', '古物商, 加賀谷, トップページ, 買取, 古物')

@section('content')

<x-navi path="purchase"></x-navi>

<div class="contents c1">
<div class="inner">

<div class="main">

<section>
    <h2>査定・買取の流れ</h2>
    <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. A reprehenderit vero similique ducimus obcaecati explicabo molestias porro praesentium amet non alias consectetur ipsum repellendus odio sed dignissimos, excepturi autem tenetur.
    </p>
</section>

<section id="input-form">

    <h2>査定依頼完了</h2>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci ab corporis obcaecati ipsa qui sapiente asperiores neque provident placeat quisquam fugiat dicta nemo sit soluta vel, veritatis et odio possimus.
    </p>

</section>



</div>
<!--/main-->

@endsection
