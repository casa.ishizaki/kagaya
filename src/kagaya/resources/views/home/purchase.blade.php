@extends('layout.home')

@section('title', '古物商 加賀谷 店主紹介')
@section('description', '古物商 加賀谷 店主紹介')
@section('keyword', '古物商, 加賀谷, トップページ, 買取, 古物')

@section('content')

<x-navi path="purchase"></x-navi>

@csrf

<div class="contents c1">
<div class="inner">

<div class="main" id="app">

    <router-view></router-view>

</div>
<!--/main-->




@endsection
