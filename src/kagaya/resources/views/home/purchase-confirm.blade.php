@extends('layout.home')

@section('title', '古物商 加賀谷 店主紹介')
@section('description', '古物商 加賀谷 店主紹介')
@section('keyword', '古物商, 加賀谷, トップページ, 買取, 古物')

@section('content')

<x-navi path="purchase"></x-navi>

<div class="contents c1">
<div class="inner">

<div class="main">

<section>
    <h2>査定・買取の流れ</h2>
    <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. A reprehenderit vero similique ducimus obcaecati explicabo molestias porro praesentium amet non alias consectetur ipsum repellendus odio sed dignissimos, excepturi autem tenetur.
    </p>
</section>

<section id="input-form">

    <h2>査定依頼</h2>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci ab corporis obcaecati ipsa qui sapiente asperiores neque provident placeat quisquam fugiat dicta nemo sit soluta vel, veritatis et odio possimus.
    </p>
    <form action="" id="item-form">
        <table class="ta1 mb1em">
            <tr>
                <th colspan="2" class="tamidashi">※マークは入力必須です</th>
            </tr>
            <tr>
                <th>お名前※</th>
            <td><input type="text" name="name" size="30" class="ws"></td>
            </tr>
            <tr>
                <th>メールアドレス※</th>
                <td><input type="email" name="email" size="30" class="ws"></td>
            </tr>
            <tr>
                <th>ご住所(都道府県)</th>
            <td>
            <select name="prefecture">
                <option value="" selected="selected">都道府県選択</option>
                <option value="北海道">北海道</option>
                <option value="青森県">青森県</option>
                <option value="岩手県">岩手県</option>
                <option value="宮城県">宮城県</option>
                <option value="秋田県">秋田県</option>
                <option value="山形県">山形県</option>
                <option value="福島県">福島県</option>
                <option value="茨城県">茨城県</option>
                <option value="栃木県">栃木県</option>
                <option value="群馬県">群馬県</option>
                <option value="埼玉県">埼玉県</option>
                <option value="千葉県">千葉県</option>
                <option value="東京都">東京都</option>
                <option value="神奈川県">神奈川県</option>
                <option value="新潟県">新潟県</option>
                <option value="富山県">富山県</option>
                <option value="石川県">石川県</option>
                <option value="福井県">福井県</option>
                <option value="山梨県">山梨県</option>
                <option value="長野県">長野県</option>
                <option value="岐阜県">岐阜県</option>
                <option value="静岡県">静岡県</option>
                <option value="愛知県">愛知県</option>
                <option value="三重県">三重県</option>
                <option value="滋賀県">滋賀県</option>
                <option value="京都府">京都府</option>
                <option value="大阪府">大阪府</option>
                <option value="兵庫県">兵庫県</option>
                <option value="奈良県">奈良県</option>
                <option value="和歌山県">和歌山県</option>
                <option value="鳥取県">鳥取県</option>
                <option value="島根県">島根県</option>
                <option value="岡山県">岡山県</option>
                <option value="広島県">広島県</option>
                <option value="山口県">山口県</option>
                <option value="徳島県">徳島県</option>
                <option value="香川県">香川県</option>
                <option value="愛媛県">愛媛県</option>
                <option value="高知県">高知県</option>
                <option value="福岡県">福岡県</option>
                <option value="佐賀県">佐賀県</option>
                <option value="長崎県">長崎県</option>
                <option value="熊本県">熊本県</option>
                <option value="大分県">大分県</option>
                <option value="宮崎県">宮崎県</option>
                <option value="鹿児島県">鹿児島県</option>
                <option value="沖縄県">沖縄県</option>
            </select></td>
            </tr>
            <tr>
                <th>ご住所(市区町村以下)</th>
                <td>
                    <input type="text" name="addres" size="30" class="wl">
                </td>
            </tr>
            <tr>
            <th>査定品目※</th>
                <td>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目１">お問い合わせ項目１</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目２">お問い合わせ項目２</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目３">お問い合わせ項目３</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目４">お問い合わせ項目４</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目５">お問い合わせ項目５</label>
                </td>
            </tr>
            <tr>
                <th>品目名</th>
                <td><input type="text" name="item_name" size="30" class="wl"></td>
            </tr>

            <tr>
                <th>画像ファイル</th>
                <td>
                    <div>(ドラッグ&ドロップでも添付できます。)</div>
                    <div id="drop-image" dropzone="file">
                        <span>ファイルをここにドラッグ&ドロップします。</span>
                    </div>
                    <input type="file" name="item-image-file" id="item-image-file" class="wl" multiple>
                    <ul id="prevew_image"></ul>
                </td>
                </tr>
            <tr>

            <tr>
                <th>希望買取価格※</th>
                <td><input type="number" name="item_price" class="wl"></td>
            </tr>

            <tr>
                <th>査定品目の詳細※</th>
                <td><textarea name="comment" cols="30" rows="10" class="wl"></textarea></td>
            </tr>

            <tr>
                <th>そのほかご要望・コメント</th>
                <td><textarea name="comment" cols="30" rows="10" class="wl"></textarea></td>
            </tr>
        </table>

        <p class="c">
            <input type="submit" value="内容を確認する">
        </p>
    </form>
</section>

<section id="confirm-form" style="display: none">

    <h2>送信内容確認</h2>
    <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci ab corporis obcaecati ipsa qui sapiente asperiores neque provident placeat quisquam fugiat dicta nemo sit soluta vel, veritatis et odio possimus.
    </p>
    <form action="/purchase/complete" id="confirm-form">
        <table class="ta1 mb1em">
            <tr>
                <th>お名前</th>
            <td>
                <span id="name-confirm"></span>
                <input type="hidden" id="name-hidden" name="name">
            </td>
            </tr>
            <tr>
                <th>メールアドレス</th>
                <td>
                    <span id="email-confirm"></span>
                    <input type="hidden" id="email-hidden" name="email">
                </td>
            </tr>
            <tr>
                <th>ご住所(都道府県)</th>
                <td>
                    <span id="prefecture-fild"></span>
                    <input type="hidden" id="prefecture-hidden" name="prefecture">
                </td>
            </tr>
            <tr>
                <th>ご住所(市区町村以下)</th>
                <td>
                    <span id="prefecture-fild"></span>
                    <input type="hidden" id="prefecture-hidden" name="prefecture">
                </td>
            </tr>
            <tr>
            <th>査定品目※</th>
                <td>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目１">お問い合わせ項目１</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目２">お問い合わせ項目２</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目３">お問い合わせ項目３</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目４">お問い合わせ項目４</label><br>
                    <label><input type="radio" name="item_type" value="お問い合わせ項目５">お問い合わせ項目５</label>
                </td>
            </tr>
            <tr>
                <th>品目名</th>
                <td><input type="text" name="item_name" size="30" class="wl"></td>
            </tr>

            <tr>
                <th>画像ファイル</th>
                <td>
                    <div>(ドラッグ&ドロップでも添付できます。)</div>
                    <div id="drop-image" dropzone="file">
                        <span>ファイルをここにドラッグ&ドロップします。</span>
                    </div>
                    <input type="file" name="item-image-file" id="item-image-file" class="wl" multiple>
                    <ul id="prevew_image"></ul>
                </td>
                </tr>
            <tr>

            <tr>
                <th>希望買取価格※</th>
                <td><input type="number" name="item_price" class="wl"></td>
            </tr>

            <tr>
                <th>査定品目の詳細※</th>
                <td><textarea name="comment" cols="30" rows="10" class="wl"></textarea></td>
            </tr>

            <tr>
                <th>そのほかご要望・コメント</th>
                <td><textarea name="comment" cols="30" rows="10" class="wl"></textarea></td>
            </tr>
        </table>

        <p class="c">
            <input type="submit" value="内容を確認する">
        </p>
    </form>
</section>

</div>
<!--/main-->

<script src="js/image_read.js" defer></script>

<script>
if (OCwindowWidth() <= 800) {
    const e = document.getElementById('drop-image');
    e.style.display = 'none';
}
</script>

@endsection
