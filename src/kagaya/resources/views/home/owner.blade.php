@extends('layout.home')

@section('title', '古物商 加賀谷 店主紹介')
@section('description', '古物商 加賀谷 店主紹介')
@section('keyword', '古物商, 加賀谷, トップページ, 買取, 古物')

@section('content')

<x-navi path="owner"></x-navi>

<div class="contents c1">
<div class="inner">

<div class="main">

<section>

<h2>掲載のご案内<span>Information</span></h2>
<p>掲載のご案内をここに入れて下さい。</p>

</section>

</div>
<!--/main-->

@endsection
