<nav id="menubar">
    <!--PC用（801px以上端末）メニュー-->

    <ul class="inner">
    <li class="{{$homeClass}}"><a href="/">ホーム</a></li>
    <li class="{{$itemsClass}}"><a href="/item">買取品目</a>
        <ul class="ddmenu">
        <li><a href="/item/1">刀装具</a></li>
        <li><a href="/item/1">文具</a></li>
        <li><a href="/item/1">ミニタリー</a></li>
        <li><a href="/item/1">ガラス</a></li>
        <li><a href="/item/1">家具</a></li>
        <li><a href="/item/1">古民具</a></li>
        <li><a href="/item/1">玩具</a></li>
        <li><a href="/item/1">古布</a></li>
        <li><a href="/item/1">着物</a></li>
        <li><a href="/item/1">中国美術</a></li>
        <li><a href="/item/1">その他骨董品</a></li>

        </ul>
    </li>
    <li class="{{$purchaseClass}}"><a href="/purchase">買取・査定</a></li>
    <li class="{{$companyClass}}"><a href="/company">店舗案内</a></li>
    <li class="{{$contactClass}}"><a href="/contact">お問い合わせ</a></li>
    </ul>
    </nav>
    <!--小さな端末用（800px以下端末）メニュー-->
    <nav id="menubar-s">
    <ul>
    <li><a href="/">ホーム</a></li>
    <li><a href="/item">買取品目</a></li>
    <li><a href="/purchase">買取・査定</a></li>
    <li><a href="/company">店舗案内</a></li>
    <li><a href="/contact">お問い合わせ</a></li>
    </ul>
</nav>
